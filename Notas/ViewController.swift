//
//  ViewController.swift
//  Notas
//
//  Created by Master Móviles on 11/01/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var mensaje: UITextView!
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var usuario: UILabel!
    @IBOutlet weak var tags: UITextField!
    
    @IBAction func createNote(_ sender: Any) {
        
        
        fecha.text = ""
        mensaje.text = ""
        usuario.text = ""
    }
    
    @IBAction func saveNote(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss d-MM-yyyy"
        let fechaGuardado = Date()
        
        if let miDelegate = UIApplication.shared.delegate as? AppDelegate {
            
            let miContexto = miDelegate.persistentContainer.viewContext
            let nuevaNota = NSEntityDescription.insertNewObject(forEntityName: "Nota", into: miContexto) as! Nota
            
            nuevaNota.contenido = mensaje.text
            nuevaNota.fecha = fechaGuardado as NSDate?
            nuevaNota.tags = (tags.text?.components(separatedBy: " "))!
            
            do{
                try miContexto.save()
                usuario.text = "Nota guardada"
                fecha.text = dateFormatter.string(from: fechaGuardado)
            }catch{
                print("Error al guardar el contexto: \(error)")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        let diccionario = ["universidad":"uni"]
        if let miDelegate = UIApplication.shared.delegate as? AppDelegate {
            let miContexto = miDelegate.persistentContainer.viewContext
            let miModelo = miDelegate.persistentContainer.managedObjectModel
            if let queryTmpl = miModelo.fetchRequestFromTemplate(withName: "Universidad", substitutionVariables: diccionario){                let results = try! miContexto.fetch(queryTmpl) as! [Nota]
                print("Hay \(results.count) resultados en la template")
                for mensaje in results {
                    print(mensaje.contenido!)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

