//
//  MigracionPesada.swift
//  Notas
//
//  Created by Master Móviles on 16/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

class MigracionPesada: NSEntityMigrationPolicy {

    override func createDestinationInstances(forSource sInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        //sacamos el contenido de la nota actual
        let notaOrigen = sInstance as! Nota
        let cad = notaOrigen.contenido
        
        //sacar una subcadena es rarito porque no se trabaja con pos. numéricas
        //sino con la clase Index
        let index = cad?.index((cad?.startIndex)!, offsetBy: 10)
        let tit = cad?.substring(to: index!)
        
        //creamos una nota de la nueva versión, no podemos usar la
        //clase Nota, esa es la antigua
        let notaDestino = NSEntityDescription.insertNewObject(forEntityName: "Nota", into: manager.destinationContext)
        notaDestino.setValue(tit, forKey: "titulo")
        //decimos que la nueva versión de "notaOrigen" es "notaDestino"
        manager.associate(sourceInstance: notaOrigen, withDestinationInstance: notaDestino, for: mapping)
    }
}
