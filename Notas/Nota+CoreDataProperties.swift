//
//  Nota+CoreDataProperties.swift
//  Notas
//
//  Created by Master Móviles on 18/01/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import CoreData


extension Nota {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Nota> {
        return NSFetchRequest<Nota>(entityName: "Nota");
    }

    @NSManaged public var fecha: NSDate?
    @NSManaged public var tags: [String]
    @NSManaged public var contenido: String?
    @NSManaged public var titulo: String?
}
