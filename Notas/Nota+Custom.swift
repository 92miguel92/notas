//
//  Nota+Custom.swift
//  Notas
//
//  Created by Master Móviles on 15/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation

extension Nota {
    //Devuelve una subcadena solo con la primera letra del texto
    var inicial: String? {
        if let textoNoNil = self.contenido {
            let pos2 = textoNoNil.index(after: textoNoNil.startIndex)
            return textoNoNil.substring(to:pos2)
        }
        else {
            return nil
        }
    }
}
